from configparser import ConfigParser
import os

from invoke import Collection
from invoke import task


@task
def serve(c, config_uri='development.ini'):
    """Run the development server."""

    print(serve.__doc__)

    # Load gunicorn options from configuration file
    config_uri = os.path.abspath(os.path.expanduser(config_uri))

    parser = ConfigParser()
    with open(config_uri, 'r') as fp:
        parser.read_file(fp)

    defaults = parser.defaults()

    gunicorn_opts = [
        f'--log-config={config_uri}',
        f'--paste={config_uri}',
        '--reload',
        '--threads=4',
        '--worker-class=gthread',
    ]

    if defaults.get('app.port'):
        gunicorn_opts.append(f'--bind=[::]:{defaults["app.port"]}')

    if defaults.get('app.ssl_crt'):
        gunicorn_opts.append(f'--certfile={defaults["app.ssl_crt"]}')

    if defaults.get('app.ssl_key'):
        gunicorn_opts.append(f'--keyfile={defaults["app.ssl_key"]}')

    if defaults.get('app.verify'):
        gunicorn_opts.append(f'--ca-certs=$={defaults["app.verify"]}')

    gunicorn_opts = ' '.join(gunicorn_opts)

    c.run(f'gunicorn {gunicorn_opts}', pty=True)


ns = Collection()
ns.add_task(serve)
