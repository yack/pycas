# -*- coding: utf-8 -*-

# PyCAS -- WSGI middleware for CAS authentication
# By: Cyril Lacoux <clacoux@easter-eggs.com>
#
# Copyright (C) 2014-2021 Cyril Lacoux, Easter-eggs
# https://gitlab.com/yack/pycas
#
# This file is part of PyCAS.
#
# PyCAS is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# PyCAS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" PyCAS utilities """

from beaker.util import NoneType
from beaker.util import verify_rules

from pycas.client import CAS_MODE_NORMAL
from pycas.client import CAS_MODE_PROXY


def coerce_params(params):
    """ Coerce configuration parameters """

    rules = [
        ('allowed_proxies', (list,), 'allowed_proxies must be a comma separated list of allowed proxies'),
        ('callback_url', (str, NoneType), 'callback_url must be a string designating a valid url'),
        ('enabled', (bool, int), 'enabled must be a boolean or an integer'),
        ('logout_local', (str, NoneType), 'logout_local must be a string designating a valid url'),
        ('logout_path', (str, NoneType), 'logout_path must be a string designating a valid url'),
        ('logout_redirect', (str, NoneType), 'logout_path must be a string designating a valid url'),
        ('mode', (str, NoneType), 'mode must be a string designating a valid CAS mode'),
        ('protect', (list,), 'protect must be a comma separated list of paths'),
        ('host', (str,), 'host must be a string designating a valid host'),
        ('path', (str, NoneType), 'path must be a string designating a valid path'),
        ('port', (int, NoneType), 'port must be an integer representing a valid port'),
        ('session.key', (str, NoneType), 'session.key must be a string designating session key'),
        ('timeout', (float, NoneType), 'timeout must be an integer representing a valid timeout in seconds'),
        ('verify', (bool, str), 'verify must be a boolean or a string'),
        ('version', (float, NoneType), 'version must be an integer representing a valid CAS version'),
    ]
    return verify_rules(params, rules)


def parse_config_options(config, prefix='pycas.', include_defaults=True):
    """ Parse configuration parameters """

    # Load default options
    if include_defaults:
        options = {
            'allowed_proxies': [],
            'callback_url': None,
            'enabled': True,
            'logout_local': None,
            'logout_path': None,
            'logout_redirect': None,
            'mode': CAS_MODE_NORMAL,
            'port': 443,
            'protect': [],
            'session.key': 'pycas.session.id',
            'timeout': None,
            'verify': False,
            'version': 2.0,
        }
    else:
        options = {}

    for key, val in config.items():
        if key.startswith(prefix):
            options[key[len(prefix):]] = val

    coerce_params(options)

    if not options['mode'] in (CAS_MODE_NORMAL, CAS_MODE_PROXY):
        raise Exception('mode must be {0} or {1}, not {2}'.format(CAS_MODE_NORMAL, CAS_MODE_PROXY, options['mode']))

    options['version'] = 'CAS {0}'.format(options['version'])

    return options
