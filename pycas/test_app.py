# -*- coding: utf-8 -*-

# PyCAS -- WSGI middleware for CAS authentication
# By: Cyril Lacoux <clacoux@easter-eggs.com>
#
# Copyright (C) 2014-2021 Cyril Lacoux, Easter-eggs
# https://gitlab.com/yack/pycas
#
# This file is part of PyCAS.
#
# PyCAS is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# PyCAS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" WSGI test application for PyCAS """


import logging
import html
import json
import traceback

import requests
from requests.cookies import cookiejar_from_dict
from webob.exc import HTTPException
from webob.exc import HTTPNotFound
from webob import Request

from pycas.client import CASAuthService


log = logging.getLogger(__name__)


SITE_TEMPLATE = """\
<!DOCTYPE html>
<html>
<head>
    <title>Welcome to PyCAS Test Application!</title>
    <meta charset="utf-8">
    <style type="text/css">
body {{
    background-color: #F4F4F4;
    color: #333333;
    font-family: Arial,Verdana,Arial,sans-serif;
    line-height: 16px;
}}
div.heading {{
    margin-bottom: 10px;
}}
div.block {{
    background-color: #FFFFFF;
    border: 1px solid #CCCCCC;
    border-radius: 5px;
    margin-top: 20px;
    padding: 10px;
}}
div.block.success {{
    background-color: #DDFFDD;
    border-color: #008800;
    color: #008800;
}}
div.block.error {{
    background-color: #FFDDDD;
    border-color: #880000;
    color: #880000;
}}
ul {{
    margin: 0;
}}
ul.navigation {{
    list-style: none;
    padding: 0;
    border-left: 4px solid #257BB2;
    padding-left: 5px;
}}
ul.navigation a {{
    color: #257BB2;
    text-decoration: none;
}}
ul.navigation a:hover {{
    color: #CC0000;
}}
div.block > div {{
    margin-top: 5px;
}}
dt {{
    font-weight: bold;
    float: left;
    width: 140px;
    text-align: right;
    padding-right: 20px;
}}
dd {{
    margin-left: 160px;
    margin-bottom: 10px;
}}
dd ul {{
    list-style: none;
    padding: 0;
}}
    </style>
</head>
<body>
    <h1>PyCAS Test Application</h1>
    <div>
        <ul class="navigation">
            <li><a href="{host_url}/secure">Go to secured area</a></li>
            <li><a href="{host_url}/services">Dialog with services</a></li>
            <li><a href="{host_url}/services?with-query=1">Dialog with services with query (GET)</a></li>
            <li><a href="{host_url}/services?with-data=1">Dialog with services with data (POST)</a></li>
            <li><a href="{host_url}/services?with-query=1&with-data=1">Dialog with services with query and data (POST)</a></li>
            <li><a href="{host_url}/">Go index</a></li>
            <li><a href="{host_url}/logout">Logout</a></li>
        </ul>
    </div>
    <div class="block">
        <div class="heading">
            <b>Page:</b> {page}
        </div>
        <dl>
            <dt>CAS mode</dt>
            <dd>{mode}</dd>

            <dt>Ticket</dt>
            <dd>{ticket}</dd>

            <dt>User</dt>
            <dd>{user}</dd>

            <dt>Attributes</dt>
            <dd>{attributes}</dd>
        </dl>
    </div>
{services}
</body>
</html>
"""

SERVICE_TEMPLATE = """\
    <div class="heading">
        <b>{name}</b>
    </div>
    <dl>
        <dt>Url</dt>
        <dd>{service}</dd>

        <dt>CAS mode</dt>
        <dd>{mode}</dd>

        <dt>User</dt>
        <dd>{user}</dd>

        <dt>Attributes</dt>
        <dd>{attributes}</dd>

        <dt>Ticket</dt>
        <dd>{ticket}</dd>

        <dt>Url</dt>
        <dd>{url}</dd>

        <dt>Method</dt>
        <dd>{method}</dd>

        <dt>GET</dt>
        <dd>{get}</dd>

        <dt>POST</dt>
        <dd>{post}</dd>
    </dl>
"""


def get_service_session(request, service, do_login=False):
    session = request.session
    cas_client = request.cas_client

    service_session = requests.Session()
    service_session.verify = cas_client.verify

    # Get service cookies from session
    session_key = '{0}::cookies'.format(service)

    service_cookies = session.get(session_key)
    if service_cookies:
        service_session.cookies = cookiejar_from_dict(service_cookies)

    elif do_login:
        service_session.get('{0}/secure'.format(service), auth=CASAuthService(cas_client))

        # Store service cookies to session
        session[session_key] = dict(service_session.cookies)

    return service_session


class TestApplication:
    """ Test application for the CAS middleware """

    def __init__(self, global_conf, **kwargs):

        self.start_response = None
        self.services = dict(
            (service.strip(), None)
            for service in kwargs['services'].split()
        ) if 'services' in kwargs else dict()

    def __call__(self, environ, start_response):

        self.start_response = start_response
        request = Request(environ)
        request.cas_client = environ.get('cas.client')
        request.session = environ.get('beaker.session')

        url_map = {
            '/': (self.do_index, SITE_TEMPLATE),
            '/logout': (self.do_logout, SITE_TEMPLATE),
            '/secure': (self.do_secure, SITE_TEMPLATE),
            '/services': (self.do_services, SITE_TEMPLATE),
        }

        if request.path_info in url_map:
            try:
                handler, template = url_map[request.path_info]

                client = request.cas_client
                result = handler(request)

                data = dict(
                    host_url=request.host_url,
                    mode=client.mode if client is not None else None,
                    ticket=client.ticket if client is not None else None,
                    user=environ.get('REMOTE_USER'),
                    attributes='<ul><li>{0}</li></ul>'.format(
                        '</li><li>'.join(
                            '{0}: {1}'.format(k, v)
                            for k, v in environ['cas.attributes'].items()
                        )
                    ) if environ.get('cas.attributes') else 'None',
                    services='',
                )

                data.update(result)
                response = template.format(**data)

            except HTTPException as exc:
                response = self.handle_exception(environ, exc)

            else:
                start_response('200 OK', [])
        else:
            response = self.handle_exception(environ, HTTPNotFound())

        return [bytes(response, 'utf-8')]

    def do_index(self, request):
        return dict(page='index')

    def do_logout(self, request):
        session = request.session

        for service in self.services:
            service_session = get_service_session(request, service)
            if not service_session.cookies:
                continue

            # Perform logout
            service_session.get('{0}/logout'.format(service))

        # Delete session
        session.delete()

        return dict(page='logout')

    def do_secure(self, request):
        request.session['user'] = request.cas_client.user
        return dict(page='secure')

    def do_services(self, request):
        services = []
        for service in self.services:
            if 'with-data' in request.params:
                data = {
                    'data_arg-1': 'value-1',
                    'data_arg-2': 'value-2',
                }
            else:
                data = None

            if 'with-query' in request.params:
                params = {
                    'query_arg-1': 'value-1',
                    'query_arg-2': 'value-2',
                }
            else:
                params = None

            try:
                # Get service session
                service_session = get_service_session(request, service, do_login=True)

                # Call service
                resp = service_session.request(
                    'POST' if data else 'GET',
                    service,
                    data=data,
                    params=params,
                )

                result = resp.json()
                for key in ('attributes', 'get', 'post'):
                    result[key] = '<ul><li>{0}</li></ul>'.format(
                        '</li><li>'.join(
                            '{0}: {1}'.format(k, v)
                            for k, v in result[key]
                        )
                    ) if result[key] else 'None'
                result['service'] = service

                html_ = SERVICE_TEMPLATE.format(**result)

                services.append('<div class="block success">{0}</div>'.format(html_))
            except Exception:
                services.append('<div class="block error"><b>Service:</b> {0}<br><br><b>Error!</b><pre>{1}</pre></div>'.format(
                    service, html.escape(traceback.format_exc())))
                log.exception('Failed to call service %s', service)

        return dict(page='services', services='\n'.join(services))

    def handle_exception(self, environ, exc):
        status = '{0} {1}'.format(exc.code, exc.title)
        self.start_response(status, [('content-type', 'text/html; charset="utf8"')])
        return exc.html_body(environ)


class TestService:
    """ Third pary service for test application """

    def __init__(self, global_conf, **kwargs):
        self.name = kwargs.get('name', '{0}@{1}'.format(self.__class__.__name__, id(self)))

    def __call__(self, environ, start_response):

        request = Request(environ)
        session = environ.get('beaker.session')
        client = environ.get('cas.client')

        if request.path_info == '/secure':
            session['user'] = client.user

        elif request.path_info == '/logout':
            session.delete()

        data = dict(
            url=request.url,
            method=request.method,
            get=list(request.GET.items()) if request.GET else None,
            post=list(request.POST.items()) if request.POST else None,
            mode=client.mode,
            name=self.name,
            user=environ.get('REMOTE_USER'),
            attributes=list(environ['cas.attributes'].items()) if environ.get('cas.attributes') else None,
            ticket=client.ticket,
        )

        start_response('200 OK', [('content-type', 'application/json charset="utf8"')])
        return [bytes(json.dumps(data), 'utf-8')]
