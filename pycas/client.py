# -*- coding: utf-8 -*-

# PyCAS -- WSGI middleware for CAS authentication
# By: Cyril Lacoux <clacoux@easter-eggs.com>
#
# Copyright (C) 2014-2021 Cyril Lacoux, Easter-eggs
# https://gitlab.com/yack/pycas
#
# This file is part of PyCAS.
#
# PyCAS is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# PyCAS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


""" CAS Client """


import logging
import re
from xml.etree import ElementTree as etree

from beaker.cache import Cache
from furl import furl
import requests
from requests.exceptions import SSLError
import urllib3
from webob.exc import HTTPTemporaryRedirect


# Global variables
CAS_VERSION_10 = 'CAS 1.0'
CAS_VERSION_20 = 'CAS 2.0'

CAS_MODE_NORMAL = 'normal'
CAS_MODE_PROXY = 'proxy'


CAS_NAMESPACES = {
    'cas': 'http://www.yale.edu/tp/cas',
}


PGT_IOU_RE = re.compile(r'^PGTIOU-[\.\-\w]+$')
PGT_RE = re.compile(r'^[PT]GT-[\.\-\w]+$')

log = logging.getLogger(__name__)


# Disable warnings about insecure HTTPS connection
urllib3.disable_warnings()


def extract_name(element):
    """ Remove namespace from tag names """
    return re.sub(r'\{.*\}', '', element.tag)


#
# Exceptions
#
class CASClientError(Exception):
    """ Base class for CAS exceptions """


class InvalidTicketError(CASClientError):
    """ Exception raised when ticket validation failed """

    def __init__(self, message, url, error=None, reason=None):
        # Calling inherited
        super(InvalidTicketError, self).__init__(message)

        self.url = url
        self.error = error
        self.reason = reason

    def __str__(self):
        # Calling inherited
        message = super(InvalidTicketError, self).__str__()

        return '{0}{1} ({2})'.format(
            message,
            ': {0}'.format(self.reason) if self.reason else '',
            self.url
        )


class PGTError(CASClientError):
    """ Exception raised when PGT is missing or invalid """


class PGTiouError(CASClientError):
    """ Exception raised when PGTiou is missing or invalid """


#
# CAS auth service
#
class CASAuthService:
    """ Authentication class to be used with requests to call third party services """

    def __init__(self, client):
        self.client = client

    def __call__(self, request):
        # Get Proxy ticket
        ticket = self.client.retrieve_pt(request.url)

        # Add proxy ticket to url
        f = furl(request.url)
        f.args['ticket'] = ticket

        request.url = f.url

        return request


#
# CAS client
#
class CASClient:
    """
    Jasig CAS authentication client for Python
    This implements CAS v1.0 and v2.0 authentication.

    Implementation was largely inspired by phpCAS source code.

    TODO: gateway feature
    """

    def __init__(self, request, host, path=None, port=None, verify=False, version=CAS_VERSION_20, timeout=None, mode=CAS_MODE_NORMAL, callback_url=None, allowed_proxies=None, **kwargs):

        self.request = request
        self.host = host
        self.path = path.strip('/') if path is not None else ''
        self.port = int(port) if port is not None else None
        self.version = version
        self.verify = verify
        self.timeout = int(timeout) if timeout is not None else None
        self.mode = mode
        self.allowed_proxies = [re.compile(pattern) for pattern in allowed_proxies] if allowed_proxies else []

        # Get session object from environ
        self.__session = request.environ.pop(kwargs['session.key'])

        # Sanity checks
        if version not in (CAS_VERSION_10, CAS_VERSION_20):
            raise CASClientError('Invalid version of CAS protocol {0}'.format(version))

        if mode not in (CAS_MODE_NORMAL, CAS_MODE_PROXY):
            raise CASClientError('Invalid client mode {0}'.format(mode))

        if self.mode == CAS_MODE_PROXY:
            if request.scheme != 'https':
                raise CASClientError("CAS proxies must be secured to use PyCAS; PGT's will not be received from the CAS server")

            # Store FQDN version of proxy callback url
            self.callback_url = '{0}/{1}'.format(request.application_url, callback_url.lstrip('/'))

            # Proxy Granting Tickets wallet
            settings = dict(
                (k[7:], v)
                for k, v in kwargs.items()
                if k.startswith('wallet.')
            )
            self.__wallet = Cache('PGTWallet', **settings)

        else:
            self.callback_url = None
            self.__wallet = None

        log.debug('CAS Client mode: %s', self.mode)

    #
    # Private methods
    #
    def __construct_url(self, path, **query):
        """
        Construct url to dialog with CAS server

        @param string path, the cas service to call
        @param dict **query, all arguments will be added as query params
        @return string url, the url for calling CAS service
        """

        f = furl(scheme='https', host=self.host, port=self.port, path='{0}/{1}'.format(self.path, path), query=query)

        return f.url

    def __query_cleanup(self):
        """
        Return url without CAS parameters

        @return string url, the cleaned url
        """

        f = furl(self.request.url)
        del f.args['ticket']

        return f.url

    def __validate_cas_10(self, service, ticket):
        """
        Validate ticket for service using CAS protocol v1.0
        User returned by CAS server is stored in session.
        This may raise an InvalidTicketError exception if ticket is invalid.

        @param string service, the service to validate tickets against to
        @param string ticket, the ticket to validate
        @return boolean authenticated, True on success
        """

        if self.mode == CAS_MODE_PROXY:
            raise CASClientError('Proxy mode is incompatible with CAS 1.0')

        # Call service validate url
        url = self.__construct_url('validate', ticket=ticket, service=service)

        try:
            resp = requests.get(url, timeout=self.timeout)
        except SSLError:
            raise InvalidTicketError('CAS 1.0 ticket not validated', url, reason='Failed to validate certificate')
        except Exception as exc:
            raise InvalidTicketError('CAS 1.0 ticket not validated', url, reason='Failed to communicate with server', error=exc)

        log.debug('RAW response\n%s\n', resp.text)

        # Read response
        result = resp.text.split('\n')
        if result[0] == 'yes':
            # Storing user to session (no attributes)
            self.__session['user'] = result[1]
            return True

        if result[0] == 'no':
            raise InvalidTicketError('Ticket not validated', url, reason='Authentication failure')

        raise InvalidTicketError('Ticket not validated', url, reason='Bad response from server')

    def __validate_cas_20(self, service, ticket):
        """
        Validate ticket for service using CAS protocol v2.0
        User and attributes returned by CAS server are stored in session.
        This may raise an InvalidTicketError exception if ticket is invalid.

        @param string service, the service to validate tickets against to
        @param string ticket, the ticket to validate
        @return boolean authenticated, True on success
        """

        # Call service validate url
        kwargs = dict(
            ticket=ticket,
            service=service,
        )
        if self.mode == CAS_MODE_PROXY:
            kwargs['pgtUrl'] = self.callback_url

        validate_uri = 'proxyValidate' if self.allowed_proxies else 'serviceValidate'

        url = self.__construct_url(validate_uri, **kwargs)

        try:
            resp = requests.get(url, timeout=self.timeout)
        except SSLError:
            raise InvalidTicketError('CAS 2.0 ticket not validated', url, reason='Failed to validate certificate')
        except Exception as exc:
            raise InvalidTicketError('CAS 2.0 ticket not validated', url, reason='Failed to communicate with server', error=exc)

        log.debug('RAW response\n%s\n', resp.text)

        # Read response
        try:
            tree = etree.fromstring(resp.text)
        except Exception:
            raise InvalidTicketError('CAS 2.0 ticket not validated', url, reason='Bad response from server')

        if extract_name(tree[0]) == 'authenticationSuccess':
            # Storing user to session
            self.__session['user'] = tree[0].find('cas:user', namespaces=CAS_NAMESPACES).text

            # Extra attributes
            attributes = tree[0].find('cas:attributes', namespaces=CAS_NAMESPACES)
            if attributes is not None:
                self.__session['attributes'] = dict(
                    (extract_name(attribute), attribute.text)
                    for attribute in attributes
                )

            # Proxies
            proxies = tree[0].find('cas:proxies', namespaces=CAS_NAMESPACES)
            if proxies is not None:
                # We are sitting behing proxies
                self.__session['proxies'] = [
                    proxy.text
                    for proxy in proxies
                ]

                # Checking all proxies
                for proxy in self.proxies:
                    allowed = False
                    for regex in self.allowed_proxies:
                        allowed = regex.match(proxy)
                        if allowed:
                            break

                    if not allowed:
                        raise InvalidTicketError('CAS 2.0 ticket not validated', url, reason='Proxy `{0}` not allowed'.format(proxy))

            if self.mode == CAS_MODE_PROXY:
                # Validate PGT
                pgt_iou = tree[0].find('cas:proxyGrantingTicket', namespaces=CAS_NAMESPACES)
                if pgt_iou is None:
                    raise InvalidTicketError('CAS 2.0 authentication error', url, reason='Ticket validated but no PGT Iou transmitted')

                pgt_iou = pgt_iou.text
                if not PGT_IOU_RE.match(pgt_iou):
                    raise InvalidTicketError('CAS 2.0 authentication error', url, reason='Invalid PGT Iou format')

                if pgt_iou not in self.__wallet:
                    raise InvalidTicketError('CAS 2.0 authentication error', url, reason='Received an invalid PGT Iou')

                # Storing pgt to session
                self.__session['pgt'] = self.__wallet[pgt_iou]

                del self.__wallet[pgt_iou]

            return True

        if extract_name(tree[0]) == 'authenticationFailure':
            raise InvalidTicketError('Ticket not validated', url, reason='Authentication failure')

        raise InvalidTicketError('Ticket not validated', url, reason='Bad response from server')

    #
    # Properties
    #
    @property
    def attributes(self):
        """ Authenticated user's attributes """

        if 'attributes' not in self.__session:
            return {}
        return self.__session['attributes'].copy()

    @property
    def pgt(self):
        """ Current PGT """

        return self.__session.get('pgt')

    @property
    def proxies(self):
        """ Authorized proxies list """

        if 'proxies' not in self.__session:
            return []
        return self.__session['proxies'][:]

    @property
    def ticket(self):
        """ Current CAS ticket """

        return self.__session.get('ticket')

    @property
    def user(self):
        """ Authenticated user """

        return self.__session.get('user')

    #
    # Public methods
    #
    def force_authentication(self):
        """
        Force authentication by redirecting user to login page if not already
        authenticated.

        This may raise an HTTPTemporaryRedirect exception which will be handled later by
        HTTPRedirectHandler middleware.

        @return boolean authenticated, True if user is authenticated
        """

        if self.is_authenticated():
            return True

        # Redirect user to login page
        url = self.__construct_url('login', service=self.request.url)
        log.debug('Not authenticated, redirecting to login page `%s`', url)
        raise HTTPTemporaryRedirect(location=url)

    def is_authenticated(self):
        """
        Check if current session is authenticated on CAS server
        If ticket parameter is present in query params, the method call the validate() method
        to ask CAS server about ticket validity.
        An HTTPTemporaryRedirect exception will be raised with a clean url if ticket is valid.
        This may raise an InvalidTicketError exception if ticket is invalid.

        @return boolean authenticated, True is user is authenticated, False otherwise
        """

        if self.user is not None:
            log.debug('User %s is already authenticated', self.user)
            return True

        if 'ticket' in self.request.params:
            # validating ticket
            self.validate()

            if not self.proxies:
                # Redirect to clean url if not proxied
                # When proxied the user will never see the url
                # This enables POST only proxied services
                log.debug('User %s is now authenticated, redirecting to clean url...', self.user)
                url = self.__query_cleanup()
                raise HTTPTemporaryRedirect(location=url)

        return self.user is not None

    def logout(self, redirect=True, redirect_service=None):
        """
        Logging out from application
        An HTTPTemporaryRedirect exception will be raised to call CAS logout service

        @param boolean redirect, whether to perform redirect to CAS logout service or not.
        @param string redirect_service, the optional service to redirect user to
        """

        self.__session.delete()

        if not redirect:
            log.debug('Local logout')
            return True

        if redirect_service:
            url = self.__construct_url('logout', service=redirect_service)
        else:
            url = self.__construct_url('logout')

        log.debug('Global logout, reditecting to logout page `%s`', url)
        raise HTTPTemporaryRedirect(location=url)

    def proxy_callback(self):
        """
        Proxy callback.
        This method collects PGT and PGT Iou from CAS server when in proxy mode.
        If pgtIou *and* pgtId are missing from query, CAS server is just
        checking SSL certificate.

        An exception may be raised if an error occurs.

        @return string result, body to return to CAS server in case of success.
        """

        if self.mode != CAS_MODE_PROXY:
            raise CASClientError('CAS client must be in proxy mode to handle proxy callback')

        pgt_iou = self.request.params.get('pgtIou')
        pgt = self.request.params.get('pgtId')

        if pgt_iou is None and pgt is None:
            # Certificate validation
            return 'Ok'

        if pgt_iou is None or not PGT_IOU_RE.match(pgt_iou):
            raise PGTiouError('PGTiou format invalid {0}'.format(pgt_iou))

        if pgt is None or not PGT_RE.match(pgt):
            raise PGTError('PGT format invalid {0}'.format(pgt))

        log.debug('Storing PGT `%s` (id=%s)', pgt, pgt_iou)

        self.__wallet[pgt_iou] = pgt

        return 'PGT Stored'

    def renew_authentication(self):
        """
        Renew authentication
        Raise an HTTPTemporaryRedirect exception to call CAS login service with renew
        argument set if already logged in.
        """

        if self.is_authenticated():
            # Renewing authentication
            url = self.__construct_url('login', service=self.request.url, renew='true')
            log.debug('Authenticated, redirecting to login page `%s`', url)

        else:
            # Normal authentication
            url = self.__construct_url('login', service=self.request.url)
            log.debug('Not authenticated, redirecting to login page `%s`', url)

        raise HTTPTemporaryRedirect(location=url)

    def retrieve_pt(self, target_service):
        """
        Retrieve Proxy Ticket for target service
        Proxy Granting Ticket must be present.
        This may raise an InvalidTicketError if any error occurs.
        A CASClientError is raised if client is not in proxy mode.
        Proxy Ticket is stored to session.

        @param string target_service, service url to retrieve Proxy Ticket for
        @return string pt, the proxy ticket for target service
        """

        if self.mode != CAS_MODE_PROXY:
            raise CASClientError('CAS client must be in proxy mode to retrieve PT')

        if self.pgt is None:
            raise InvalidTicketError('Failed to retrieve PT', None, reason='Missing PGT')

        # Call proxy url
        url = self.__construct_url('proxy', pgt=self.pgt, targetService=target_service)

        try:
            resp = requests.get(url, timeout=self.timeout)
        except SSLError:
            raise InvalidTicketError('Failed to retrieve PT', url, reason='Failed to validate certificate')
        except Exception as exc:
            raise InvalidTicketError('Failed to retrieve PT', url, reason='Failed to communicate with server', error=exc)

        log.debug('RAW response\n%s\n', resp.text)

        # Read response
        try:
            tree = etree.fromstring(resp.text)
        except Exception:
            raise InvalidTicketError('Failed to retrieve PT', url, reason='Bad response from server')

        if extract_name(tree[0]) == 'proxySuccess':
            pt = tree[0].find('cas:proxyTicket', namespaces=CAS_NAMESPACES)
            if pt is None:
                raise InvalidTicketError('Failed to retrieve PT', url, reason='Ticket validated but no PT transmitted')

            # Store PT in session
            if 'pt' not in self.__session:
                self.__session['pt'] = dict()
            self.__session['pt'][target_service] = pt.text

            return pt.text

        if extract_name(tree[0]) == 'proxyFailure':
            raise InvalidTicketError('Failed to retrieve PT', url, reason='Proxy failure')

        raise InvalidTicketError('Failed to retrieve PT', url, reason='Bad response from server')

    def validate(self):
        """
        Validate CAS ticket
        Ticket must be present in query params.
        This may raise an InvalidTicketError exception if ticket is invalid.

        @return boolean authenticated, True on success
        """

        ticket = self.request.params.get('ticket')
        if ticket is None:
            raise CASClientError('Missing ticket')

        if 'ticket' in self.__session:
            del self.__session['ticket']

        # Construct service url
        service = self.__query_cleanup()

        log.debug('%s ticket `%s` is present', self.version, ticket)

        if self.version == CAS_VERSION_10:
            self.__validate_cas_10(service, ticket)

        elif self.version == CAS_VERSION_20:
            self.__validate_cas_20(service, ticket)

        else:
            raise CASClientError('Invalid version of CAS protocol {0}'.format(self.version))

        # Store valid ticket to session
        self.__session['ticket'] = ticket

        log.debug('%s ticket `%s` was validated', self.version, ticket)

        if self.mode == CAS_MODE_PROXY:
            log.debug('PGT `%s` was validated', self.pgt)

        return True
