# PyCAS

PyCAS is a WSGI middleware for CAS authentication.


## Development
The project is managed using [Poetry](https://poetry.eustace.io/docs/#installation):

```
# Create virtualenv
mkdir .venv
python3 -m venv .venv

# Activate virtualenv
source .venv/bin/activate

# Install Poetry
pip install poetry

# Run web server in development mode
poetry run invoke serve
```

## Installation

```
pip install pycas
```


## Useful documentation

* [CAS Protocol](docs/CAS Protocol.pdf)
* [CAS Proxy Protocol](docs/CAS Proxy Protocol.pdf)
